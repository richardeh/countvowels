"""
Count the vowels in an input string

Author: Richard Harrington
Created: 9/4/2013
Last updated: 9/4/2013
"""

Vowels=["a","e","i","o","u"]
vowelCount=0
aCount=0
eCount=0
iCount=0
oCount=0
uCount=0

s=input("Input a string:\n")

for c in s:
    if c.lower() in Vowels:
        vowelCount+=1
        if c.lower()=="a":
            aCount+=1
        elif c.lower()=="e":
            eCount+=1
        elif c.lower()=="i":
            iCount+=1
        elif c.lower()=="o":
            oCount+=1
        else:
            uCount+=1
        
print("There are",str(vowelCount),"vowels in your string.\n")
print("There are",str(aCount),"a\'s\n")
print("There are",str(eCount),"e\'s\n")
print("There are",str(iCount),"i\'s\n")
print("There are",str(oCount),"o\'s\n")
print("There are",str(uCount),"u\'s\n")
